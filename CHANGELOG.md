# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.12

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.11

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.10

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.

## 0.3.9

- patch: Update the Readme with details about LOCAL_PATH parameter.

## 0.3.8

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.7

- patch: Improve cloud formation stack template

## 0.3.6

- patch: Update README.md with modifying permissions example

## 0.3.5

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.3.4

- patch: Update pipes bash toolkit version.

## 0.3.3

- patch: Updated contributing guidelines

## 0.3.2

- patch: FIX issue with large writes to stdout failing with 'Resource temporarily unavailable'

## 0.3.1

- patch: Standardising README and pipes.yml.

## 0.3.0

- minor: Force the git ssh command used when pushing a new tag to override the default SSH configuration for piplines.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines SFTP pipe.

